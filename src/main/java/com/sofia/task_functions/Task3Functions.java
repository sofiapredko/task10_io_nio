package com.sofia.task_functions;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Task3Functions {
    private static final Logger LOG = LogManager.getLogger(Task3Functions.class);
    private static final String FILE_COMMENTS = "C:\\Users\\Sofia\\Documents\\Arr.txt";
    private static final String FILE_COMMENTS_1 = "C:\\Users\\Sofia\\Documents\\Arrays.java";

    public static void comments(String fileName) throws IOException {
        List<String> list = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            list = stream
                    .map(line -> line.trim())
                    .filter(line -> line.startsWith("//") || line.startsWith("/*") || line.startsWith(" "))
                    .collect(Collectors.toList());

        } catch (IOException e) {
            LOG.error("Error!" + e);
        }
        list.forEach(System.out::println);
        LOG.info("Finded all comments in file " + fileName);
    }

    public static void run() throws IOException {
        comments(FILE_COMMENTS_1);
        comments(FILE_COMMENTS);
    }
}


