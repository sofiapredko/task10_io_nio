package com.sofia.task_functions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Task6Functions {
    private static final Logger LOG = LogManager.getLogger(Task6Functions.class);
    public static final String DIR_NAME = "C:\\Users\\Sofia\\Documents\\projectlog";

    /**
     * Function to list all files from a directory and its subdirectories.
     *
     * @param directoryName path to directory
     */

    public static void listAllFilesEverywhere(String directoryName) {
        File directory = new File(directoryName);

        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                System.out.println(file.getAbsolutePath());
            } else if (file.isDirectory()) {
                listAllFilesEverywhere(file.getAbsolutePath());
            }
        }
    }

    /**
     * List all the files and folders from a directory
     *
     * @param directoryName path to directory
     */

    public static void listAllFilesAndFolders(String directoryName) {
        File directory = new File(directoryName);

        File[] fileList = directory.listFiles();
        for (File file : fileList) {
            System.out.println(file.getName());
        }
        LOG.info("Successfully! Displayed all files and folders in dir " + directoryName);
    }

    /**
     * List all the files and folders from a directory using streams
     *
     * @param directoryName path to directory
     */

    public static void listAllFilesByStream(String directoryName) throws IOException {
        Stream<Path> files = Files.list(Paths.get(directoryName));
        files.forEach(System.out::println);
        files.close();
        LOG.info("Successfully! Displayed all files and folders in dir using stream " + directoryName);
    }

    public static void run() throws IOException {
        System.out.println("All: ");
        Task6Functions.listAllFilesEverywhere(DIR_NAME);
        System.out.println("All files and folders: ");
        Task6Functions.listAllFilesAndFolders(DIR_NAME);
        System.out.println("All files and folders stream: ");
        Task6Functions.listAllFilesByStream(DIR_NAME);
    }

}
