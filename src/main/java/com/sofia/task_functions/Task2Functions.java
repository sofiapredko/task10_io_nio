package com.sofia.task_functions;

import java.io.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Task2Functions {
    private static final Logger LOG = LogManager.getLogger(Task2Functions.class);
    private static final double CONVERT_TIME_NUMBER = 1000000000.0;
    private static final File FILE_NAME_1 = new File("C:\\Users\\Sofia\\Documents\\AT\\стартап кр реферат.docx");
    private static final File FILE_NAME_2 = new File("C:\\Users\\Sofia\\Documents\\AT\\lab_4.pdf");
    private static final File FILE_NAME_3 = new File("C:\\Users\\Sofia\\Documents\\Books QA\\Kaner_testirovanie_programmnogo_obespecheeniia.pdf");
    private static final String TEXT_TO_INSERT = "Helllo from nowhere";
    private static final int DEFAULT_BUFFER_SIZE = 8 * 1024; //8192 default for buffer
    private static final int BUFFER_SIZE_1 = 100 * 1024;

    public static void testInputStream(File fileName) {
        try (InputStream input = new FileInputStream(fileName)) {
            long start = System.nanoTime();
            int data = input.read();
            while (data != -1) {
                data = input.read();
            }
            long end = System.nanoTime();
            LOG.info("Done InputStream: " + fileName + ", size: " + fileName.length());

            long elapsedTime = end - start;
            double seconds = (double) elapsedTime / CONVERT_TIME_NUMBER;
            LOG.info("Time processing: " + seconds + " seconds");

        } catch (IOException e) {
            LOG.error("Error!!!!");
        }
    }

    @SuppressWarnings("not duplicate")
    public static void testOutputStream(File fileName, String text) {
        try (OutputStream output = new FileOutputStream(fileName)) {
            long start = System.nanoTime();
            output.write(text.getBytes());
            long end = System.nanoTime();
            LOG.info("Done OutputStream: " + fileName + ", size: " + fileName.length());

            long elapsedTime2 = end - start;
            double seconds = (double) elapsedTime2 / CONVERT_TIME_NUMBER;
            LOG.info("Time processing: " + seconds + " seconds");

        } catch (IOException e) {
            LOG.error("Error!!!!" + e);
        }
    }

    @SuppressWarnings("not duplicate")
    public static void testBufferReaderStream(File fileName, int bufferSize) {
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(fileName)), bufferSize)) {
            long start = System.nanoTime();
            int content = br.read();
            while (content != -1) {
                content = br.read();
            }
            long end = System.nanoTime();
            LOG.info("Done BufferReaderStream: " + fileName + ", size: " + fileName.length());

            long elapsedTime2 = end - start;
            double seconds = (double) elapsedTime2 / CONVERT_TIME_NUMBER;

            LOG.info("Size of buffer: " + bufferSize);
            LOG.info("Time processing: " + seconds + " seconds");
        } catch (
                IOException e) {
            LOG.error("Error!!!!");
            //e.printStackTrace();
        }
    }


    @SuppressWarnings("not duplicate")
    public static void testBufferWriterStream(File fileName, int bufferSize, String text) {
        try {
            FileOutputStream fileIn = new FileOutputStream(fileName);
            BufferedOutputStream bufferIn = new BufferedOutputStream(fileIn);
            DataOutputStream dataIn = new DataOutputStream(bufferIn);
            long start = System.nanoTime();
            dataIn.writeChars(text);
            long end = System.nanoTime();
            LOG.info("Done BufferWriterStream" + fileName + ", size: " + fileName.length());

            long elapsedTime2 = end - start;
            double seconds = (double) elapsedTime2 / CONVERT_TIME_NUMBER;
            LOG.info("Time processing: " + seconds + " seconds");
        } catch (
                IOException e) {
            LOG.error("Error!!!!");
        }
    }

    public static void run() {
        testInputStream(FILE_NAME_1);
        testInputStream(FILE_NAME_2);
        testInputStream(FILE_NAME_3);
        testOutputStream(FILE_NAME_1, TEXT_TO_INSERT);
        testBufferReaderStream(FILE_NAME_1, DEFAULT_BUFFER_SIZE);
        testBufferReaderStream(FILE_NAME_2, DEFAULT_BUFFER_SIZE);
        testBufferReaderStream(FILE_NAME_3, DEFAULT_BUFFER_SIZE);
        testBufferReaderStream(FILE_NAME_3, BUFFER_SIZE_1);
        testBufferWriterStream(FILE_NAME_1, DEFAULT_BUFFER_SIZE, TEXT_TO_INSERT);
    }


}
