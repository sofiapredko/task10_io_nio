package com.sofia.task_functions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Scanner;

public class Task4Functions {
    private static final Logger LOG = LogManager.getLogger(Task4Functions.class);
    private static Scanner scanner = new Scanner(System.in);
    private static final int BUFFER_SIZE = 1024;

    public static void testPushBackSream() throws IOException {
        try {
            System.out.println("Input string please: ");
            String inputString = scanner.nextLine();
            LOG.info("Your String: " + inputString);

            byte[] bytesArr = inputString.getBytes();
            ByteArrayInputStream byteArr = new ByteArrayInputStream(bytesArr);
            PushbackInputStream pushBackStream = new PushbackInputStream(byteArr, BUFFER_SIZE);

            char ch = (char) pushBackStream.read();
            LOG.info("Unread first byte... ");
            System.out.println("First character of PushbackInputStream pushBackStream is " + ch);

            pushBackStream.unread(ch);
            int inBytes = pushBackStream.available();
            LOG.info("pushBackStream has " + inBytes + " available bytes");

            LOG.info("Printing your String as PushbackInputStream:::");
            byte[] inBuf = new byte[inBytes];
            for (int i = 0; i < inBytes; ++i) {
                inBuf[i] = (byte) pushBackStream.read();
                System.out.print((char) inBuf[i]);
            }

            System.out.println();
            LOG.info("Unread array of bytes: ");
            final int length = 6;
            pushBackStream.unread(bytesArr, 0, length);
            for (int i = 0; i < length; ++i) {
                inBuf[i] = (byte) pushBackStream.read();
                System.out.print((char) inBuf[i]);
            }

        } catch (IOException i) {
            LOG.error("Something went wrong!");

        }
    }
}
