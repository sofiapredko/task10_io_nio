package com.sofia.task_functions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.*;
import java.io.*;
import java.nio.channels.*;

public class SomeBufferTask7 {
    private File classFile;
    private static final Logger LOG = LogManager.getLogger(SomeBufferTask7.class);
    private static final File FILE_NAME = new File("C:\\Users\\Sofia\\Documents\\binary tree map.txt");
    private static final File NEW_FILE_NAME = new File("C:\\Users\\Sofia\\Documents\\(NEW) binary tree map.txt");

    public SomeBufferTask7() {
    }

    public SomeBufferTask7(File classFile) {
        this.classFile = classFile;
    }

    public File getClassFile() {
        return classFile;
    }

    public void setClassFileName(File classFile) {
        this.classFile = classFile;
    }

    public void copyToNewFile(File newFile) throws IOException {
        LOG.info("Copy data: ");
        FileInputStream inputStream = new FileInputStream(getClassFile());
        FileOutputStream outputStream = new FileOutputStream(newFile);
        FileChannel inChannel = inputStream.getChannel();
        FileChannel outChannel = outputStream.getChannel();
        try {
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } catch (IOException e) {
            LOG.error("Error with copying files!" + e);
        } finally {
            inChannel.close();
            outChannel.close();
            inputStream.close();
            outputStream.close();
        }
        LOG.info("Data was copied from " + classFile.getName() + " to" + newFile);
    }


    public void readFileChannel() throws IOException {
        RandomAccessFile aFile = new RandomAccessFile(getClassFile(), "rw");
        FileChannel inChannel = aFile.getChannel();
        final int buffer = 48;
        ByteBuffer buf = ByteBuffer.allocate(buffer);
        int bytesRead = inChannel.read(buf);
        LOG.info("Was read " + bytesRead + " bytes in " + classFile.getName());
        LOG.info("Buffer: " + buffer + "path: " + classFile.getPath());
        while (bytesRead != -1) {
            buf.flip();
            /*
            while (buf.hasRemaining()) {
                System.out.print((char) buf.get());
            }
            */
            buf.clear();
            bytesRead = inChannel.read(buf);
        }
        aFile.close();
    }


    public void writeToFileChannel() throws IOException {
        RandomAccessFile aFile = new RandomAccessFile(classFile, "rw");
        FileChannel channel = aFile.getChannel();
        String newData = "New String to write to classFile...\n" + System.currentTimeMillis();
        final int buffer = 8000;

        ByteBuffer buf = ByteBuffer.allocate(buffer);
        buf.clear();
        final byte[] src = newData.getBytes();
        buf.put(src);
        buf.flip();

        while (buf.hasRemaining()) {
            channel.write(buf);
        }
        LOG.info("To classFile through channel: " + classFile.getName() + ", buffer " + buffer);
        LOG.info("New String was written to classFile: " + newData);
        channel.close();
    }

    public static void run() throws IOException {
        SomeBufferTask7 o = new SomeBufferTask7();
        o.setClassFileName(FILE_NAME);
        o.copyToNewFile(NEW_FILE_NAME);
        o.readFileChannel();
        o.writeToFileChannel();
    }

}
