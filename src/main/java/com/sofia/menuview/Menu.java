package com.sofia.menuview;

import com.sofia.serialization_task1.Task1Functions;
import com.sofia.task_functions.*;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.logging.log4j.Logger;

final public class Menu {
    private Map<String, String> menu;
    private Map<String, Functional> methodsMenu;
    private static final Scanner INPUT = new Scanner(System.in);
    private static final Logger LOG = LogManager.getLogger(Menu.class);

    private Menu() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - Test Serializable.");
        menu.put("2", " 2 - Compare reading and writing files using usual and buffer reader of different size");
        menu.put("3", " 3 - Test PuchBackInputStream ");
        menu.put("4", " 4 - Find comments in a Java source code file");
        menu.put("5", " 5 - Get contents of directories");
        menu.put("6", " 6 - Test SomeBuffer class");
        menu.put("0", " 0 - Exit.");

        methodsMenu.put("1", Task1Functions::taskSerialization);
        methodsMenu.put("2", Task2Functions::run);
        methodsMenu.put("3", Task4Functions::testPushBackSream);
        methodsMenu.put("4", Task3Functions::run);
        methodsMenu.put("5", Task6Functions::run);
        methodsMenu.put("6", SomeBufferTask7::run);
        methodsMenu.put("0", this::exitFromProgram);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    private void exitFromProgram() {
        LOG.debug("SUCCESSFULLY END");
        System.exit(0);
    }

    public static void runMenu() {
        String key;
        Menu menu = new Menu();
        while (true) {
            try {
                System.out.println("\n");
                menu.outputMenu();
                System.out.println("Please, select menu point");
                key = INPUT.nextLine();
                System.out.println("\n\n");
                menu.methodsMenu.get(key).start();
            } catch (NullPointerException e) {
                LOG.warn("\nChoose correct choice please");
            } catch (IOException e) {
                LOG.error("\nSomething wrong with files!");
            } catch (ClassNotFoundException e) {
                LOG.error("\nCan't find class");
            } catch (Exception e) {
                LOG.error("ERROR!!!");
            }
        }
    }
}
