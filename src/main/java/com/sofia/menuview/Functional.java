package com.sofia.menuview;

import java.io.IOException;

@FunctionalInterface
public interface Functional {
    void start() throws IOException, ClassNotFoundException;
}
