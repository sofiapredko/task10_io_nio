package com.sofia.clientserver_chat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

public class ChatClientThread extends Thread {
    private static final Logger LOG = LogManager.getLogger(ChatClientThread.class);
    private Socket socket;
    private ChatClient client;
    private DataInputStream streamIn;

    public ChatClientThread(ChatClient client, Socket socket) {
        this.client = client;
        this.socket = socket;
        open();
        start();
    }

    private void open() {
        try {
            streamIn = new DataInputStream(socket.getInputStream());
        } catch (IOException ioe) {
            LOG.error("Error getting input stream: " + ioe);
            client.stop();
        }
    }

    public void close() {
        try {
            if (streamIn != null) {
                streamIn.close();
            }
        } catch (IOException ioe) {
            LOG.error("Error closing input stream: " + ioe);
        }
    }

    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                client.handle(streamIn.readUTF());
            } catch (IOException ioe) {
                LOG.fatal("Listening error: " + ioe.getMessage());
                client.stop();
            }
        }
    }
}
