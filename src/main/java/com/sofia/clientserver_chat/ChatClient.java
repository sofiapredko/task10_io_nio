package com.sofia.clientserver_chat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class ChatClient implements Runnable {
    private static final Logger LOG = LogManager.getLogger(ChatClient.class);
    private Socket socket;
    private Thread thread;
    private DataInputStream console;
    private DataOutputStream streamOut;
    private ChatClientThread client;

    public ChatClient(String serverName, int serverPort) {
        System.out.println("Establishing connection. Please wait ...");
        try {
            socket = new Socket(serverName, serverPort);
            LOG.info("Connected: " + socket);
            start();
        } catch (UnknownHostException uhe) {
            LOG.error("Host unknown: " + uhe.getMessage());
        } catch (IOException ioe) {
            LOG.error("Unexpected exception: " + ioe.getMessage());
        }
    }

    public void run() {
        while (thread != null) {
            try {
                streamOut.writeUTF(console.readLine());

                streamOut.flush();
            } catch (IOException ioe) {
                LOG.error("Sending error: " + ioe.getMessage());
                stop();
            }
        }
    }

    public void handle(String msg) {
        if (msg.equals(".bye")) {
            System.out.println("Good bye. Press RETURN to exit ...");
            stop();
        } else {
            System.out.println(msg);
        }
    }

    public void start() throws IOException {
        console = new DataInputStream(System.in);
        streamOut = new DataOutputStream(socket.getOutputStream());
        if (thread == null) {
            client = new ChatClientThread(this, socket);
            thread = new Thread(this);
            thread.start();
        }
    }

    public void stop() {
        if (thread != null) {
            thread.interrupt();
            thread = null;
        }

        try {
            if (console != null) {
                console.close();
            }
            if (streamOut != null) {
                streamOut.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException ioe) {
            LOG.error("Error closing ...");
        }

        client.close();
        client.interrupt();
    }

    public static void main(String args[]) {
        ChatClient client = new ChatClient("localhost", 4000);
    }
}
