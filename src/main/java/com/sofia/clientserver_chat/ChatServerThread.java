package com.sofia.clientserver_chat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;

public class ChatServerThread extends Thread {
    private static final Logger LOG = LogManager.getLogger(ChatServerThread.class);
    private ChatServer server;
    private Socket socket;
    private DataInputStream streamIn;
    private DataOutputStream streamOut;
    private int ID;

    public ChatServerThread(ChatServer server, Socket socket) {
        super();
        this.server = server;
        this.socket = socket;
        this.ID = socket.getPort();
    }

    public void send(String msg) {
        try {
            streamOut.writeUTF(msg);
            streamOut.flush();
        } catch (IOException ioe) {
            LOG.error(ID + " ERROR sending: " + ioe.getMessage());
            server.remove(ID);
            interrupt();
            // stop();
        }
    }

    public int getID() {
        return ID;
    }

    public void run() {
        LOG.info("Server Thread " + ID + " running.");

        while (!Thread.currentThread().isInterrupted()) {
            try {
                server.handle(ID, streamIn.readUTF());
            } catch (IOException ioe) {
                LOG.fatal(ID + " ERROR reading: " + ioe.getMessage());
                server.remove(ID);
                interrupt();
                // stop();
            }
        }
    }

    public void open() throws IOException {
        streamIn = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
        streamOut = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
    }

    public void close() throws IOException {
        if (socket != null) {
            socket.close();
        }
        if (streamIn != null) {
            streamIn.close();
        }
        if (streamOut != null) {
            streamOut.close();
        }
    }
}
