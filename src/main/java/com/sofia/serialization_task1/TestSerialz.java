package com.sofia.serialization_task1;

import java.io.Serializable;

public class TestSerialz implements Serializable {
    private String cityName;
    private int population;
    private ParentClass countryName;
    private transient int cityId;

    public TestSerialz(String cityName, int population, ParentClass countryName, int cityId) {
        this.cityName = cityName;
        this.population = population;
        this.countryName = countryName;
        this.cityId = cityId;
    }

    @Override
    public String toString() {
        return "City {" + "name='" + cityName + '\'' + ", population='" + population + '\''
                + ", located in country " + countryName.getName()
                + ", id: " + cityId + " }";
    }

    public ParentClass getCountryName() {
        return countryName;
    }

    public String getCityName() {
        return cityName;
    }
}
