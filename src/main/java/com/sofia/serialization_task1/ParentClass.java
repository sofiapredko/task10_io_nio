package com.sofia.serialization_task1;

import java.io.Serializable;

public class ParentClass implements Serializable {
    private String name;

    public ParentClass(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
