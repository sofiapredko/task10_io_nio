package com.sofia.serialization_task1;

import java.io.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Task1Functions {
    private static final Logger LOG = LogManager.getLogger(Task1Functions.class);

    public static void taskSerialization() throws IOException, ClassNotFoundException {
        ParentClass country1 = new ParentClass("USA");
        TestSerialz city1 = new TestSerialz("New York", 15, country1, 67005);
        TestSerialz city2 = new TestSerialz("Seattle", 12, country1, 69035);

        ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new FileOutputStream("cities.out"));
        objectOutputStream.writeObject(city1);
        objectOutputStream.writeObject(city2);
        objectOutputStream.close();
        LOG.info("Successfully serialized " + city1.getCountryName() + " " + city1.getCityName());
        LOG.info("Successfully serialized " + city2.getCountryName() + " " + city2.getCityName());

        ObjectInputStream objectInputStream = new ObjectInputStream(
                new FileInputStream("cities.out"));
        TestSerialz cityDe1 = (TestSerialz) objectInputStream.readObject();
        TestSerialz cityDe2 = (TestSerialz) objectInputStream.readObject();
        objectInputStream.close();

        LOG.info("Before Serialization: " + "\n" + city1 + "\n" + city2);
        LOG.info("After Deserialization : " + "\n" + cityDe1 + "\n" + cityDe2);
    }

}
